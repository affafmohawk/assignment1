# README #

Assignment 1

### What needs to happen? ###

* Create a functional Login page, that checks to ensure the username and password is correct. Provides an error message if incorrect.
*The main.php file has an outline of what needs to be done, only replace within the 'CHANGE' blocks

### Steps for working with this ###

* Fork the repository, make sure you set it as private!
* Using source tree, download the sample project
* Make your changes
* Push to your repository
* Remember to add me as a user to your repository so I can mark it

The above steps are recorded in a video